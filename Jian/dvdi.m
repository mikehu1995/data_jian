% This Skrip is for calculating R0;
% The required data is the complete current and voltage data of each trip;
% If want to automatically calculate the R0 of all trips, 
% first arrange the file names of each trip in numerical order.
% R0_trace records the R0 value of all trips;
% Jian Hu
% 29/1/2022

clear;
clc;
R0_trace = []; % Track R0 changes across all trips

amount = 498; %number of the trips

    for i = 1:498
        if exist(['C:\Users\mikeh\OneDrive\桌面\SOH/datareset/' ...
                num2str(i) 'Cell6.mat'])==0  %Ignore trips that do not meet the requirements and do not exist in the analysis data
            continue;   
        end
        load (['C:\Users\mikeh\OneDrive\桌面\SOH/datareset/' ...
                num2str(i) 'Cell6.mat']);  %load the trip data
        C01 = data.current;                %Current data with sampling-rate 0.1s.
        C05 = data.current(1:5:end);       %Current data with sampling-rate 0.5s.
        C1 = data.current(1:10:end);       %Current data with sampling-rate 1s.
        C10 = data.current(1:100:end);     %Current data with sampling-rate 10s.
        V01 = data.voltage;                %Voltage data with sampling-rate 0.1s.
        V05 = data.voltage(1:5:end);
        V1 = data.voltage(1:10:end);
        V10 = data.voltage(1:100:end);
        C = C1;                            % hier choose the sampling-rate to the data,which will  be analysed.
        V = V1;
        tpunkt = [];                       % Mark the points that meet the requirements
        for k = 2:length(C)
            if abs(C(k)-C(k-1)) >= 30      % when the delta Current bigger than 30A, mark the point as tpunkt.（>0.5C)
                tpunkt = [tpunkt,k];
            end
        end
        if size(tpunkt,1) == 0             % when this trip has no point, than ignore this trip.
            continue;
        end
        deltaC = C(tpunkt)' - C((tpunkt-1))';
        deltaV = V(tpunkt)' - V((tpunkt-1))';
        R_vor = deltaV./deltaC;             % calculate the row Resistance.
        R0 = movmean(R_vor,length(R_vor));  % use the move mean to reduce the influence of extreme values
        R0_mean = mean(R0);                 % Take the mean as R0
        save([ 'C:\Users\mikeh\OneDrive\桌面\SOH\SOHR/results/' num2str(i)...
            '.mat'],"R0_mean","R_vor","deltaC",...
            "deltaV","tpunkt","R0","V","C")
        R0_trace = [R0_trace, R0_mean];     % in the end show the change of Resistance of all trips
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%hu = movmean(R0_trace,length(R0_trace));
%yyaxis left
%plot(C)
%yyaxis right
%plot(V)
%grid on

